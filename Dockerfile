# Usar una imagen base con Ruby 3.1.0
FROM ruby:3.1.0

# Instalar paquetes básicos
RUN apt-get update -qq && apt-get install -y nodejs npm

# Crear y establecer el directorio de trabajo
RUN mkdir /myapp
WORKDIR /myapp

# Copiar el Gemfile y Gemfile.lock y ejecutar bundle install
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install

# Copiar el package.json y package-lock.json (si existe) y ejecutar npm install
COPY package.json /myapp/package.json

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash && \
    export NVM_DIR="$HOME/.nvm" && \
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" && \
    nvm install 18.18.0 && \
    nvm use 18.18.0 && \
    npm install

# Copiar el resto de la aplicación
COPY . /myapp

# Ejecutar comandos de configuración de la base de datos
RUN bundle exec rake db:create db:migrate db:seed

# Exponer el puerto 3000 y ejecutar el servidor
EXPOSE 3000
RUN chmod +x bin/*
RUN chmod 0664 /myapp/log/development.log
CMD ["./bin/dev"]
